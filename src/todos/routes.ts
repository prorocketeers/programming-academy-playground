import Router from 'koa-router'
import 'koa-body'
import { Context } from 'koa'
import { Todo } from './models'

const routes = new Router({
    prefix: '/todos'
})

let todos: Array<Todo> = []

// define routes
// create one Todo
routes.post('/', (ctx: Context) => {
    const todo: Todo = ctx.request.body
    todo.id = Date.now()
    todos.push(todo)

    ctx.response.status = 201
    ctx.response.body = todo
})
// get all todos
routes.get('/', (ctx: Context) => {
    ctx.response.body = todos
})
// get 1 todo
routes.get('/:id', (ctx: Context) => {
    const id = Number(ctx.params.id)
    for(let todo of todos) {
        if(todo.id === id) {
            ctx.response.body = todo
            break;
        }
    }
    if(!ctx.response.body) {
        ctx.response.status = 404
    }
})

// update Todo
routes.put('/:id', (ctx: Context) => {
    // HW
    // find and replace whole todo in todos array
})

// delete Todo
routes.delete('/:id', (ctx: Context) => {
    const id = Number(ctx.params.id)
    todos = todos.filter((todo: Todo) => {
        return todo.id !== id
    })

    ctx.response.status = 200
})

export default routes