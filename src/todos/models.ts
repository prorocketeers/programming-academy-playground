
export interface Todo {
    id?: number
    message: string
    isDone: boolean
    color: 'red' | 'white' | 'green'
}
