process.on('uncaughtException', (error) => {
    console.error(error)
})

// const http = require('http')
import http, { IncomingMessage, ServerResponse, Server } from 'http'
import requestHandler from './requestHandler';
import { port } from './config';



const server: Server = http.createServer(requestHandler)

server.listen(port, (error) => {
  if(error) {
    console.error(error)
  }
  else {
    console.log('It\'s running!')
  }
})
