class Counter {
  count: number = 0

  increment() {
    this.count++
  }
}

export default Counter