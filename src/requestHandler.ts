import { IncomingMessage, ServerResponse } from "http";
import { port } from "./config";
import Counter from "./Counter";

let counter = new Counter()

function requestHandler(
  request: IncomingMessage, 
  response: ServerResponse
  ) {
  console.log('Url: ' + request.url)
  console.log(`Url: ${request.url}`)
  console.log('port: '+port)

  counter.increment()
  response.end(counter.count.toFixed())
}

export default requestHandler