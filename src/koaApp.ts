import logger from 'koa-logger'
import Router from 'koa-router'
import koaBody from 'koa-body'
import Koa, { Context } from 'koa'

import todoRoutes from './todos/routes'

const port = 3000
const app = new Koa()

app.use(logger())
app.use(koaBody())

app.use(todoRoutes.routes())
app.listen(port, () => {
    console.log(`App is running.`)
})
